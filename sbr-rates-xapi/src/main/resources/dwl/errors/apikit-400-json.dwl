%dw 2.0
output application/json
var errorMessage = p('secure::errorKeys.'++("BAD_REQUEST" as String))
var errorDescription = p('secure::errorDescription.message')
---
{
	"error":
	{
		"errorMessage": errorMessage, 
		"errorDesc": errorDescription,
		"errorKey": "BAD_REQUEST",
    	"errorCode": "400"
	}
}
