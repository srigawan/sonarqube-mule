%dw 2.0
output application/json
var errorDescription = p('secure::errorDescription.message')
---
{
	"error":
	{	
		"errorMessage": "Unsupported media type",
		"errorDesc": errorDescription,
		"errorKey": "UNSUPPORTED_MEDIA_TYPE",
    	"errorCode": "415"
	}
}