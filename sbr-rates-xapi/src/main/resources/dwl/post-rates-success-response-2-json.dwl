%dw 2.0
output application/json
---
{
    "data":
    {
    	"status":{
    		"code": payload.data.errorCode,
    		"desc": payload.data.errorDesc
    	},
    	"company": payload.data.company,
        "customer": payload.data.customer,
        "jobsite": payload.data.jobsite,
        "location": payload.data.location,
        "items":payload.data.items map(item,index) -> {
              "catId": item.catId,
              "classId": item.classId,
			  "productId": item.catId ++ item.classId,
			  "rateFlag":item.rateFlag,
			  "rateFlagDescription":item.rateFlagDescription,
			  "notToExceed": item.notToExceed,
			  "notToExceedDescription": item.notToExceedDescription,
			  "rppPercentage": item.rppPercentage,
			  "needPassword": item.needPassword,
			  "showAllRates": item.showAllRates,
			  "suggestMinimalHours": item.suggestMinimalHours default null,
			  "isConsumerCatClass": item.isConsumerCatClass default false,
			  "isSerialized": item.isSerialized default false,
			  "customerDiscountPercentageDaily": item.customerDiscountPercentageDaily,
			  "customerDiscountPercentageWeekly": item.customerDiscountPercentageWeekly,
			  "customerDiscountPercentageMonthly": item.customerDiscountPercentageMonthly,
			  "customerMonthly": item.customerMonthly,
			  "rates":if(item.rates.segmentedRates.start ==null and item.rates.segmentedRates.target ==null and item.rates.segmentedRates.floor ==null )
			  item.rates - "segmentedRates"
			  else 
		      item.rates
         }
    }
}
