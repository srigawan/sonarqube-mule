%dw 2.0
output application/json skipNullOn="everywhere"
---
{
    "data":
    {
        "errorCode": payload.data.status.code,
        "errorDesc": trim(payload.data.status.desc),
        "company": payload.data.company,
        "customer": payload.data.customer,
        "jobsite": payload.data.jobsite,
        "location": payload.data.location,
        "items": payload.data.items map(item,index)->{
              "catId": item.catId,
              "classId": item.classId,
			  "productId": item.catId ++ item.classId,
			  "rateFlag":item.rateFlag,
			  "notToExceed": item.notToExceed,
			  "rppPercentage": item.rppPercentage,
			  "rateFlagDescription":item.rateFlagDescription,
			  "needPassword": item.needPassword,
			  "showAllRates": item.showAllRates,
			  "suggestMinimalHours": item.suggestHoursForMinimum default null,
			  "isConsumerCatClass": item.isConsumerCatClass default false,
			  "isSerialized": item.isSerialized default false,
			  "notToExceedDescription": item.notToExceedDescription,
			  "customerDiscountPercentageDaily": item.customerDiscountPercentageDaily,
			  "customerDiscountPercentageWeekly": item.customerDiscountPercentageWeekly,
			  "customerDiscountPercentageMonthly": item.customerDiscountPercentageMonthly,
			  "customerMonthly": item.customerMonthly,
		      "rates":{
		        "suggestedRates":{
		            "minimum":item.suggestMinimum,
		            "daily":item.suggestDaily,
		            "weekly":item.suggestWeekly,
	                "monthly":item.suggestMonthly
		            },
		         "segmentedRates":{            
                    "start": 
                    if(item.zilliantOptimalDaily ==  ".00" or item.zilliantOptimalDaily == null)	
                    null
                    else {
	                     "minimum":item.suggestMinimum,
	                     "daily":item.zilliantOptimalDaily,
	                     "weekly":item.zilliantOptimalWeekly,
			             "monthly":item.zilliantOptimalMonthly
		                  },                       
		             "target":
                     if(item.zilliantBalancedDaily ==  ".00" or item.zilliantBalancedDaily == null) 
                     null
                     else{
		                "minimum":item.suggestMinimum,
		                "daily":item.zilliantBalancedDaily,
		                "weekly":item.zilliantBalancedWeekly,
		                "monthly":item.zilliantBalancedMonthly
		                  },                          
		            "floor":
                    if(item.zilliantFloorDaily ==  ".00" or item.zilliantFloorDaily == null) 
                          null
                          else{
		                     "minimum":item.suggestMinimum,
		                     "daily":item.zilliantFloorDaily,
		                     "weekly":item.zilliantFloorWeekly,
		                     "monthly":item.zilliantFloorMonthly
		                  } 
       		            },
		       "contractualRates":
		               	if(item.rateFlag=="Y" and item.notToExceed=="S")
		                  "set":{
		                     "minimum":item.suggestMinimum,
		                     "daily":item.suggestDaily,
		                     "weekly":item.suggestWeekly,
		                     "monthly":item.suggestMonthly
		                  }
		              else if(item.rateFlag=="Y" and item.notToExceed=="X")
		                  "DNE":{
		                     "minimum":item.suggestMinimum,
		                     "daily":item.suggestDaily,
		                     "weekly":item.suggestWeekly,
		                     "monthly":item.suggestMonthly
		                  }
		             else if((item.rateFlag=="1" or item.rateFlag=="2" or item.rateFlag=="3" ) and item.notToExceed=="")
		                  "loaded":{
		                     "minimum":item.suggestMinimum,
		                     "daily":item.suggestDaily,
		                     "weekly":item.suggestWeekly,
		                     "monthly":item.suggestMonthly
		                  }
		             else if(item.rateFlag=="Y" and item.notToExceed=="P")
		                  "offLocal":{
		                     "minimum":item.suggestMinimum,
		                     "daily":item.suggestDaily,
		                     "weekly":item.suggestWeekly,
		                     "monthly":item.suggestMonthly
		                  }
		             else if(item.rateFlag=="Y" and item.notToExceed=="")
		                   "specialRates":{
		                    "minimum":item.suggestMinimum,
		                     "daily":item.suggestDaily,
		                     "weekly":item.suggestWeekly,
		                     "monthly":item.suggestMonthly
		                  }
		                  else
		                  null,          
		               "bookRates":{
		                  "minimum":item.bookMinimum,
		                  "daily":item.bookDaily,
		                  "weekly":item.bookWeekly,
		                  "monthly":item.bookMonthly
		               }
		            }
    		}
    }
 }