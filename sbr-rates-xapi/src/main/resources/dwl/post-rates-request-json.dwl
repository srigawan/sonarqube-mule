%dw 2.0
output application/json skipNullOn="everywhere"
---
{
  	"company": attributes.headers["companyId"] as String,
    "location": payload.pc as String,
    "customer": payload.customerNumber default null,
    "jobSite": payload.jobSite default null,
    "ratesFor":  payload.items map(item,index) ->{
			      	"catId": item[0 to 2],
			      	"classId":item[3 to 6]
      }
  }