%dw 2.0
output application/json
var query = {
	"input" : attributes.queryParams['input'] default "",
	"types" : attributes.queryParams['types'] default "",
	"location" : attributes.queryParams['location'] default "",
	"radius" : attributes.queryParams['radius'] default "",
	"components" : attributes.queryParams['components'] default ""
	}
var logPayload = write(query , "application/json")
---
"More Details , input/query params received :: flow name : get-autosuggest :: " ++ logPayload