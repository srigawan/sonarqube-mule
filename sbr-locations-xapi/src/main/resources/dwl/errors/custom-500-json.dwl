%dw 2.0
output application/json
var errorMessage = p('secure::errorKeys.'++("INTERNAL_SERVER_ERROR" as String))
var errorDescription = p('secure::errorDescription.message')
---
{
	"error":	
	{
    	"errorMessage": errorMessage,
    	"errorDesc": errorDescription,
    	"errorKey": "INTERNAL_SERVER_ERROR",
    	"errorCode": "500"
    }  	
}