%dw 2.0
output application/json
var errorDescription = p('secure::errorDescription.message')
---
{
	"error":
	{	
		"errorMessage": "Not acceptable",
		"errorDesc": errorDescription,
		"errorKey": "NOT_ACCEPTABLE",
    	"errorCode": "406"
	}
}