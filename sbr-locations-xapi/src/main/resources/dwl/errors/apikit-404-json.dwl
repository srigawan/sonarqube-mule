%dw 2.0
output application/json
var errorMessage = p('secure::errorKeys.'++("NOT_FOUND" as String))
var errorDescription = p('secure::errorDescription.message')
---
{
	"error":
	{
		"errorMessage": errorMessage,
		"errorDesc": errorDescription,
		"errorKey": "RESOURCE_NOT_FOUND",
    	"errorCode": "404"
	}
}