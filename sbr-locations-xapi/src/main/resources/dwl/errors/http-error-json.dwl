%dw 2.0
output application/json
var errorIdentifier= error.errorType.identifier
var errorDescription = p('secure::errorDescription.message')
---
{
	"error":
	{
    	"errorMessage": errorIdentifier,
    	"errorDesc": errorDescription,
    	"errorKey": "INTERNAL_SERVER_ERROR",
    	"errorCode": "500"
	}
}