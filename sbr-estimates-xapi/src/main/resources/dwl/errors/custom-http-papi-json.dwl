%dw 2.0
output application/json
var errorKey = if (
(error.errorMessage.attributes.statusCode as String default '' contains("504")) 
	
) ('SERVICE_AVAILABILITY')
else if ((vars.errorPayload.message as String default '' contains("No HTTP resource was found")) 
) Mule::p('secure::errorKeys.NOT_FOUND')
else if ((vars.errorPayload.message as String default '' contains("Invalid security headers"))
) Mule::p('secure::errorKeys.UNAUTHORIZED')
else
Mule::p('secure::errorKeys.INTERNAL_SERVER_ERROR')

var errorCode = 
 if (
(error.errorMessage.attributes.statusCode as String default '' contains("504")) 
	
) ('500')
	

else if ((vars.errorPayload.message as String default '' contains("No HTTP resource was found"))
	
) Mule::p('secure::errorStatusCode.NOT_FOUND')
else if ((vars.errorPayload.message as String default '' contains("Invalid security headers"))
) Mule::p('secure::errorStatusCode.UNAUTHORIZED')

else
Mule::p('secure::errorStatusCode.INTERNAL_SERVER_ERROR')

var suberrorcode=if (
(error.errorMessage.attributes.statusCode as String default '' contains("504")) 
	
) ('004') else ('000')
var errorDescription = Mule::p('secure::errorDescription.message')
---
{
"error":
{
"errorMessage": payload.error.errorMessage default "INTERNAL_SERVER_ERROR",
"errorDesc": errorDescription,
"errorKey": payload.error.errorKey default errorKey,
"errorCode":if (payload.error.errorCode default"" contains("-")) payload.error.errorCode else (payload.error.errorCode default 500 ++ "-" ++ suberrorcode as String)
}
}