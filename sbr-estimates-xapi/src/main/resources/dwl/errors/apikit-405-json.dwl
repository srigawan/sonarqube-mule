%dw 2.0
output application/json
var errorDescription = p('secure::errorDescription.message')
---
{
	"error":
	{
		"errorMessage": "Method not allowed",
		"errorDesc": errorDescription,
		"errorKey": "METHOD_NOT_ALLOWED",
    	"errorCode": "405"
	
	}
}
