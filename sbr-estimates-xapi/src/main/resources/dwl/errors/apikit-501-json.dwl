%dw 2.0
output application/json
var errorDescription = p('secure::errorDescription.message')
---
{
	"error":
	{	
		"errorMessage": "Not Implemented",
		"errorDesc": errorDescription,
		"errorKey": "NOT_IMPLEMENTED",
    	"errorCode": "415"
	}
}