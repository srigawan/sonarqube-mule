%dw 2.0
import * from dw::core::Strings
output application/json

var status = payload.data.status

var product= payload.data.estimate.catClassCharges.items default []
var sales= payload.data.estimate.salesItemCharges.salesItems default []
var misc=payload.data.estimate.itemizedCharges.items default []
var totals= payload.data.estimate.totals default {}
var taxInfo=payload.data.estimate.taxInfo default {}


---
data:{

"status": {code:status.code as String ++ "-000",
desc:lower(status.desc)
},
"estimate":{
  itemizedCharges:{
    //products:product,
    products:product map (item) ->{
 (item mapObject (value, key) ->{
     (((key) replace "D" with "d"): value) if(key as String == 'catID' or key as String == 'classID'),
 	((key):value) if(key as String !='catID' and key as String != 'classID')

 }
 )
 },
     //products: product map (item) ->{
			//	(item mapObject (value, key) ->{
			//		((replaceAll((key),"D","d")): value) if(key as String == 'catID' or key as String == 'classID'),
				//	((key):value) if(key as String !='catID' and key as String != 'classID')
//})
			//},
    salesItems:sales
                 },
  miscCharges: misc,
  totals:totals,
  taxInfo:taxInfo,
  otherDetails: {
    "distBetweenPCandSite":
       payload.data.distanceFromJobSiteAndPC,
    "unitOfDistance": "miles",
    "unitofCurrency": taxInfo.currencyCode


}
}}